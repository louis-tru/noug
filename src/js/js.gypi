{
	'variables': {
		'libs_noug_files_out': [
			'libs/noug/out/noug/_action.js',
			'libs/noug/out/noug/_common.js',
			'libs/noug/out/noug/_event.js',
			'libs/noug/out/noug/_ext.js',
			'libs/noug/out/noug/_path.js',
			'libs/noug/out/noug/_pkgutil.js',
			'libs/noug/out/noug/_util.js',
			'libs/noug/out/noug/_view.js',
			'libs/noug/out/noug/action.js',
			'libs/noug/out/noug/app.js',
			'libs/noug/out/noug/buffer.js',
			'libs/noug/out/noug/checkbox.js',
			'libs/noug/out/noug/css.js',
			'libs/noug/out/noug/ctr.js',
			'libs/noug/out/noug/dialog.js',
			'libs/noug/out/noug/display_port.js',
			'libs/noug/out/noug/event.js',
			'libs/noug/out/noug/font.js',
			'libs/noug/out/noug/fs.js',
			'libs/noug/out/noug/http.js',
			'libs/noug/out/noug/media.js',
			'libs/noug/out/noug/nav.js',
			'libs/noug/out/noug/overlay.js',
			'libs/noug/out/noug/package.json',
			'libs/noug/out/noug/path.js',
			'libs/noug/out/noug/pkg.js',
			'libs/noug/out/noug/index.js',
			'libs/noug/out/noug/reader.js',
			'libs/noug/out/noug/stepper.js',
			'libs/noug/out/noug/storage.js',
			'libs/noug/out/noug/sys.js',
			'libs/noug/out/noug/timer.js',
			'libs/noug/out/noug/util.js',
			'libs/noug/out/noug/value.js',
			'libs/noug/out/noug/errno.js',
		],
	},
	'targets': [
	{
		'target_name': 'noug-v8',
		'type': 'none',
		'dependencies': [
			'deps/v8-link/v8-link.gyp:v8-link',
			'deps/v8-link/v8-link.gyp:v8_libplatform-link',
		],
	},
	{
		'target_name': 'noug-node',
		'type': 'static_library', #<(output_type)
		'dependencies': [
			'noug',
			'noug-js',
			'deps/node/node.gyp:node',
		],
		"sources": [
			'../../tools/useless.c', # fix mac platform build error
		],
	},
	{
		'target_name': 'noug-js',
		'type': 'static_library', #<(output_type)
		'include_dirs': [
			'../..',
			'../../out',
			'../../deps/node/deps/uv/include',
			'../../deps/node/deps/openssl/openssl/include',
		],
		'dependencies': [
			'noug',
			'noug-v8',
			'build_libs_noug_',
		],
		'direct_dependent_settings': {
			'include_dirs': [ '../..' ],
			'mac_bundle_resources': [],
		},
		'defines': [ 'NODE_WANT_INTERNALS=1' ],
		'sources': [
			'_js.h',
			'cls.cc',
			'js.cc',
			'js.h',
			'str.h',
			'str.cc',
			'v8.cc',
			'value.h',
			'value.cc',
			'wrap.cc',
			'node.cc',
			'start.cc',
			# api 
			'api/_cb.h',
			'api/_event.h',
			'api/_fs.h',
			'api/_json.h',
			'api/_view.h',
			'api/_view.cc',
			'api/action-frame.cc',
			'api/action.cc',
			'api/app.cc',
			'api/audio-player.cc',
			'api/background.cc',
			'api/binding.cc',
			'api/box.cc',
			'api/buffer.cc',
			'api/cb.cc',
			'api/console.cc',
			'api/css.cc',
			'api/display-port.cc',
			'api/div.cc',
			'api/event.cc',
			'api/font.cc',
			'api/fs-path.cc',
			'api/fs-reader.cc',
			'api/fs.cc',
			'api/http.cc',
			'api/hybrid.cc',
			'api/image.cc',
			'api/indep.cc',
			'api/input.cc',
			'api/json.cc',
			'api/label.cc',
			'api/layout.cc',
			'api/limit.cc',
			'api/media.cc',
			'api/os.cc',
			'api/root.cc',
			'api/scroll.cc',
			'api/span.cc',
			'api/sprite.cc',
			'api/storage.cc',
			'api/text-font.cc',
			'api/text-node.cc',
			'api/text.cc',
			'api/timer.cc',
			'api/util.cc',
			'api/video.cc',
			'api/view.cc',
		],
		'conditions': [
			['v8_enable_inspector==1', { 'defines': [ 'HAVE_INSPECTOR=1' ] }],
			['node_use_openssl=="true"', { 'defines': [ 'HAVE_OPENSSL=1' ] }],
			['node_use_dtrace=="true"', { 'defines': [ 'HAVE_DTRACE=1' ] }],
		],
		# actions
		'actions': [
			{
				'action_name': 'gen_inl_js_natives',
				'inputs': [
					'../../tools/gen-js-natives.js',
					'../../out/_event.js',
					'../../out/_value.js',
				],
				'outputs': [
					'../../out/native-inl-js.h',
					'../../out/native-inl-js.cc',
				],
				'action': [
					'<(node)',
					'<@(_inputs)',
					'',
					'INL',
					'wrap',
					'<@(_outputs)',
				],
				'process_outputs_as_sources': 1,
			},
			{
				'action_name': 'gen_lib_js_natives',
				'inputs': [
					'../../tools/gen-js-natives.js',
					'../../out/_pkg.js',
					'../../out/_pkgutil.js',
				],
				'outputs': [
					'../../out/native-lib-js.h',
					'../../out/native-lib-js.cc',
				],
				'action': [
					'<(node)',
					'<@(_inputs)',
					'',
					'LIB',
					'wrap',
					'<@(_outputs)',
				],
				'process_outputs_as_sources': 1,
			},
			{
				'action_name': 'gen_ext_js_natives_',
				'inputs': [
					'../../tools/gen-js-natives.js',
					'<@(libs_noug_files_out)',
				],
				'outputs': [
					'../../out/native-ext-js.h',
					'../../out/native-ext-js.cc',
				],
				'action': [
					'<(node)',
					'<@(_inputs)',
					'noug',
					'EXT',
					'',
					'<@(_outputs)',
				],
				'process_outputs_as_sources': 1,
			},
		],
	},
	{
		'target_name': 'build_libs_noug_',
		'type': 'none',
		'actions': [{
			'action_name': 'build',
			'inputs': [
				'../../libs/noug/tsconfig.json',
				'../../libs/noug/_action.ts',
				'../../libs/noug/_common.ts',
				'../../libs/noug/_event.ts',
				'../../libs/noug/_ext.ts',
				'../../libs/noug/_path.ts',
				'../../libs/noug/_pkgutil.ts',
				'../../libs/noug/_util.ts',
				'../../libs/noug/_view.ts',
				'../../libs/noug/action.ts',
				'../../libs/noug/app.ts',
				'../../libs/noug/buffer.ts',
				'../../libs/noug/checkbox.tsx',
				'../../libs/noug/css.ts',
				'../../libs/noug/ctr.ts',
				'../../libs/noug/dialog.tsx',
				'../../libs/noug/display_port.ts',
				'../../libs/noug/event.ts',
				'../../libs/noug/font.ts',
				'../../libs/noug/fs.ts',
				'../../libs/noug/http.ts',
				'../../libs/noug/media.ts',
				'../../libs/noug/nav.tsx',
				'../../libs/noug/overlay.tsx',
				'../../libs/noug/package.json',
				'../../libs/noug/path.ts',
				'../../libs/noug/pkg.ts',
				'../../libs/noug/index.ts',
				'../../libs/noug/reader.ts',
				'../../libs/noug/stepper.tsx',
				'../../libs/noug/storage.ts',
				'../../libs/noug/sys.ts',
				'../../libs/noug/timer.ts',
				'../../libs/noug/util.ts',
				'../../libs/noug/value.ts',
				'../../libs/noug/errno.ts',
			],
			'outputs': [
				'../out/_event.js',
				'../out/_value.js',
				'../out/_pkg.js',
				'../out/_pkgutil.js',
				'<@(libs_noug_files_out)',
			],
			'action': [ 'sh', '-c', 'cd libs/noug; npm run build' ],
		}],
	}
	],

	'conditions': [
		['os!="ios"', {
			'targets+': [
			{
				'target_name': 'noug_exec',
				'product_name': 'noug',
				'type': 'executable',
				'dependencies': [
					'noug',
					'noug-js',
					'noug-media',
					'noug-node',
				],
				'sources': [
					'main.cc',
				],
				'ldflags': [ '<@(other_ldflags)' ],
			}],
		}]
	],
}